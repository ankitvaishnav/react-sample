import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import { Provider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import Login from './layouts/login';
import Register from './layouts/register';
import AuthHOC from './authHoc';

const alertOptions = {
  position: 'top center',
  timeout: 5000,
  offset: '30px',
  transition: 'fade',
  zIndex: 9999
}

export default (
  <Provider template={AlertTemplate} {...alertOptions}>
    <Switch>
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register} />
      <Route path="/" component={AuthHOC} />
    </Switch>
  </Provider>
);
