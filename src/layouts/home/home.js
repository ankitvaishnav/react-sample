/* Libraries */
import React, {Fragment} from 'react';
import { Link } from "react-router-dom";
import MaterialIcon from 'material-icons-react';
import { withAlert } from 'react-alert';

/* styles */
import './style.css';

/* Miscellaneous */
import { Functions } from './../../misc';


class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {
  }

  render() {
    return (
      <Fragment>
        <div className="page-container sp-home">
          <div className="page-content-wrapper ">
            <div className="content sm-gutter">
              <div>
                <div className="container-fluid p-l-25 p-r-5 p-b-10 sm-padding-10">
                  <div className="inner">
                    <h1 className="main-heading">Home</h1>
                  </div>
                </div>
              </div>
              <div className="container-fluid p-l-25 p-r-5 p-t-0 p-b-25 sm-padding-10">
                <div className="w-100">
                  <div className="card card-default story-card border-theme-blue cp" onClick={() => this.props.alert.info("I am here to show you how onClick works. check the code!!!")}>
                    <div className="card-header">
                      &zwnj;
                    </div>
                    <div className="card-body p-t-20 p-b-50 text-center cp" >
                      <MaterialIcon icon="add" size={32} color="#505dff" />
                      <p className="text-center text-theme-blue">Click to see Alert</p>
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <p className="text-center">
                <Link to="/login" className="text-info small">go to login</Link>
              </p>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withAlert(Home);
