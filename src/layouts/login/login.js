/* Libraries */
import React from 'react';
import { Redirect, Link } from "react-router-dom";
import { withAlert } from 'react-alert';

/* styles */
import './style.css';

/* Miscellaneous */
import { Functions, Constants } from './../../misc';


class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      fields:{
        email: '',
        password: '',
      },
      loading: true,
      errors: {},

    }

  }


  componentDidMount() {
    let that=this
    // Functions.checkLogin(function(err, res){
    //   if(!err){
    //     that.setState({...that.state, loading:false, user: res})
    //   }else{
    //     switch(err){
    //       case 401:
    //         that.props.history.push('/login');
    //       break;
    //       default:
    //       break;
    //     }
    //   }
    // })
    that.setState({...that.state, loading:false})
  }
  gvalid(){
    let email = this.state.fields["email"];
    let errors = {};
    if(!/@gmail\.com$/.test(email)){
      //alert('nn');
    errors["email"] =  (<div class="alert alert-danger" role="alert">
                 Only Gmail allowed!
                        </div>);
          //return false;
    }
    this.setState({errors: errors});
  }

  formSubmit(e){
    e.preventDefault();
    let that=this;
    if(this.gvalid()){
      return;
    }

    //TODO send login request here with username and password
    // this push to home page
    //this.props.history.push('/home')
  }

  handleChange(field, e){
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({fields});
  }




  render() {

    if(this.state.loading){
      return (null);
    }else if(Object.keys(this.state.user).length > 0){
      return (<Redirect to='/home'/>);
    }
    return (
      <div className="login-container full-height sm-p-t-30 bg-white">
        <div className="d-flex justify-content-center flex-column full-height ">
          <img src={Constants.logo} alt="Swiftpad" data-src={Constants.logo} data-src-retina={Constants.logo} width="150"/>
          <h4 className="p-t-20">Secure sign in. Enter your credentials below.</h4>
          <form id="form-login" className="p-t-15" onSubmit= {this.formSubmit.bind(this)}>
            <div className="form-group form-group-default">
              <label>Email</label>
              <div className="controls">
                <input type="email" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} name="email" placeholder="Type your Email address" className="form-control" required/>
                <span className="errors">{this.state.errors["email"]}</span>
              </div>
            </div>
            <div className="form-group form-group-default">
              <label>Password</label>
              <div className="controls">
                <input type="password" onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]} className="form-control" name="Enter Password" placeholder="Password" required/>
              </div>
            </div>
            <button className="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>

              <Link to="/register" className="text-info small text-center">Register here</Link>

          </form>
          <hr />
        </div>
      </div>
    );
  }
}

export default withAlert(Login);
