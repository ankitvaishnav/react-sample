/* Libraries hello is there any change */
import React, { Fragment } from 'react';
import { Route, Link, Redirect, Switch } from "react-router-dom";
import { withAlert } from 'react-alert';

/* Components */
import Home from './layouts/home';

/* Miscellaneous */
import { Functions } from './misc';

class AuthHOC extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      user: {},
      loading: true
    }
  };

  componentDidMount() {
    let that=this
    /**
    Functions.checkLogin(function(err, res){
      if(!err){
        that.setState({...that.state, loading:false, user: res})
      }else{
        switch(err){
          case 401:
            that.props.history.push('/login');
          break;
          default:
          break;
        }
      }
    })
    **/
    that.setState({...that.state, loading:false, user: {id: 12, name: 'Alice', userType: 'guest'}})
  }

  render () {
    if(this.state.loading){
      return (null);
    }else if(Object.keys(this.state.user).length === 0){
      return (<Redirect to='/login'/>);
    }else{
      return (
        <Fragment>
          <Switch>
            <Route path="/home" component={Home} user={this.state.user}/>
            <Redirect from='/' to='/home'/>
          </Switch>
        </Fragment>
      )
    }
  }

}

export default withAlert(AuthHOC);
