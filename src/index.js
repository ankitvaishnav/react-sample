/* Libraries */
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from "react-router-dom";

/* styles */
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/pages.scss';
import './assets/css/pages-icons.css';
import './assets/css/common.css';
import './assets/css/common-sm.css';
import './assets/css/common-md.css';
import './assets/css/common-lg.css';

// import registerServiceWorker from './registerServiceWorker';
import { unregister } from './registerServiceWorker';
import routes from './routes';

unregister();

ReactDOM.render(
  <Router>
    {routes}
  </Router>,
  document.getElementById('root')
);
