/* Libraries */
import Axios from 'axios';

export default function request(url, method, fields={}, cb) {
  Axios({method: method, url: process.env.REACT_APP_API_URL+url, data: fields, withCredentials: true})
  .then(function (response) {
    return cb(null, response.data);
  })
  .catch(function (error) {
    console.log(error)
    if (error.response) {
      return cb(error.response.status, error.response.data);
    } else if (error.request) {
      return cb(600, 'Connection error');
    } else {
      return cb(600, 'Connection error');
    }
  });
}
