export const logo = require('../assets/images/swift.png');
export const avatar = require('../assets/images/avatar.svg');
export const menu = require('../assets/svg/menu.svg');
export const more_vertical = require('../assets/svg/threedots.svg');
export const down_arrow = require('../assets/svg/downarrow.svg');
export const smile = require('../assets/svg/smile.svg');

export const imageRoute = process.env.REACT_APP_API_URL+'resource/protected'
